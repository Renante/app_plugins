﻿angular.module("umbraco").controller("MillenniumPrices.MPEditController",
    function ($scope, $routeParams, fileUploadService, notificationsService, navigationService) {
        $scope.fileSelected = function (files) {
            // In this case, files is just a single path/filename
            $scope.file = files;
        };

        $scope.uploadFile = function () {
            if (!$scope.isUploading) {
                if ($scope.file) {
                    $scope.isUploading = true;
                    fileUploadService.uploadFileToServer($scope.file)
                        .then(function (response) {
                            if (response) {
                                notificationsService.success("Success", "Saved to server with the filename " + response);
                            }
                            $scope.isUploading = false;
                        }, function (reason) {
                            notificationsService.error("Error", "File import failed: " + reason.message);
                            $scope.isUploading = false;
                        });
                } else {
                    notificationsService.error("Error", "You must select a file to upload");
                    $scope.isUploading = false;
                }
            }
        };

        $scope.file = false;
        $scope.isUploading = false;
    });
