﻿angular.module("umbraco").controller("TextareaPlus.controller",
    function ($scope) {
        $scope.style = "";
        
        if ($scope.model.config.trows === null || $scope.model.config.trows === "") {
            $scope.model.config.trows = 5;
        }
        if ($scope.model.config.tcols !== null && $scope.model.config.tcols !== "") {
            $scope.style = "width:" + $scope.model.config.tcols + "px;min-width:" + $scope.model.config.tcols + "px;";
        }
});