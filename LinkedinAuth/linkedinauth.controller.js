﻿angular.module("umbraco").controller("linkedinauth.controller",
    function ($scope, $location, $http) {

        var param = $location.search();

        $scope.error = param.error;
        $scope.errorDescription = param.error_description;
        $scope.tokenHasExpired = true;

        $http.get("/umbraco/backoffice/api/linkedinauthapi/GetAuthInfo")
        .then(function (response) {

            if (response.data.Token !== null) {
                var expireIn = response.data.Token.expires_in;
                var requestDate = new Date(response.data.Token.RequestedDate);
                var tokenExpireDate = new Date(requestDate.getTime() + (expireIn * 1000));

                $scope.tokenExpireDate = tokenExpireDate;
                $scope.tokenHasExpired = new Date() >= tokenExpireDate;
            }

            $scope.clientId = response.data.ClientId;
            $scope.returnUrl = response.data.ReturnUrl;

        });

    });