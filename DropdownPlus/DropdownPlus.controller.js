﻿angular.module("umbraco").controller("DropdownPlus.controller",
    function ($scope) {
        if ($scope.model.config.width === null || $scope.model.config.width === "") {
            $scope.model.config.width = 300;
        }

        if ($scope.model.value === null || $scope.model.value === "") {
            $scope.model.value = $scope.model.config.defaultValue;
        }
    });